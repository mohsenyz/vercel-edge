import { type NextRequest, NextResponse } from 'next/server'
import { verifyAuth } from '@lib/auth'

// export const config = {
//   matcher: [ '/*' ],
// }

export async function middleware(request: NextRequest) {
  const ip = request.headers.get('cf-connecting-ip') || request.headers.get('x-forwarded-for');
  let url = new URL(request.url);
  const worker_domain=url.hostname;
  url.hostname = "wpm-worker.mohsenyz.ir";     
  url.protocol = request.headers.get('x-forwarded-proto') || "https";
  let req = new Request(url, request);
  if (ip)
    request.headers.set('cf-connecting-ip', ip);
    request.headers.set('Host', worker_domain);
  
  return await fetch(req)
}
