export const USER_TOKEN = 'user-token'

const JWT_SECRET_KEY: string | undefined = process.env.JWT_SECRET_KEY!

export function getJwtSecretKey(): string {
  return JWT_SECRET_KEY ?? ''
}
